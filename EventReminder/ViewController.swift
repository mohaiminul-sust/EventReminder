//
//  ViewController.swift
//  EventReminder
//
//  Created by Mohaiminul Islam on 2/14/16.
//  Copyright © 2016 infancyit. All rights reserved.
//

import UIKit
import CalendarView

class ViewController: UIViewController {

    @IBOutlet var calendar: CalendarView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

